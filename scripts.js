function sumaFraccion(){
    var num1 = parseInt(document.getElementById("num1").value);
    var den1 = parseInt(document.getElementById("den1").value);
    var num2 = parseInt(document.getElementById("num2").value);
    var den2 = parseInt(document.getElementById("den2").value);
    var num = ((num1 * den2) + (num2 * den1));
    var den = den1 * den2;
    var restDecimal = resultadoDecimal(num, den);
    document.getElementById("numRest").innerHTML = num;
    document.getElementById("denRest").innerHTML = den;
    document.getElementById("resDec").innerHTML = restDecimal;
    var elementos = document.getElementsByClassName("opera");
    etiqueta(elementos, "suma");
    document.getElementById("resultDiv").style.display = "block";
}

function etiqueta(elemento, etiqueta){
    for(var i=0; i<elemento.length; i++){
        document.getElementsByClassName("opera")[i].innerHTML = etiqueta;
    }
}

function resultadoDecimal(num, den){
    return num/den;
}

function multiplicacion(){
    var nume1 = parseInt(document.getElementById("num1").value);
    var deno1 = parseInt(document.getElementById("den1").value);
    var nume2 = parseInt(document.getElementById("num2").value);
    var deno2 = parseInt(document.getElementById("den2").value);
    document.getElementById("numRest").innerHTML = (nume1 * nume2);
    document.getElementById("denRest").innerHTML = (deno1 * deno2);
    document.getElementById("resDec").innerHTML = resultadoDecimal((nume1 * nume2) / (deno1 * deno2));
    var elementos = document.getElementsByClassName("opera");
    etiqueta(elementos, "multiplicación");
    document.getElementById("resultDiv").style.display = "block";
}
